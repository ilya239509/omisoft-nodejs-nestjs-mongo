import * as bcrypt from 'bcrypt';

export class Password {
  static async hashPassword(password: string) {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
  }

  static async comparePassword(
    password: string,
    storedPassword: string,
  ): Promise<boolean> {
    return await bcrypt.compareSync(password, storedPassword);
  }
}
