import { createParamDecorator } from '@nestjs/common';
import { IUser } from '../types/user';

export const GetUser = createParamDecorator((req, data): IUser => {
  const request = data.switchToHttp().getRequest();
  return request?.user;
});
