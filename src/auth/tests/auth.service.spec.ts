import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from '../Auth.service';
import { getModelToken } from '@nestjs/mongoose';
import { User } from '../schemas/user.schema';
import { Model } from 'mongoose';
import { RegisterDto } from '../dto/register.dto';
import { responses } from '../../constants/responses';
import { HttpException, HttpStatus } from '@nestjs/common';

const registerUserDto: RegisterDto = {
  email: 'ilyasdqweqwe@gmail.com',
  password: 'sdsdsd12312wqe312',
};

const mockSavedUser = {
  _id: '63e2299315370734507eeb20',
  email: 'ilyasdqweqwe@gmail.com',
  password: '$2a$10$Y4qA2jDrPweo5479.qTovOeM.zc/b5wE.llxfHjHwkBkUABCrTYcG',
  __v: 0,
};

const mockCreateUser = {
  email: 'qqsdaaaa11111@asdqqqq.com',
  password: 'sdsdsd12312wqe312',
};

describe('AuthService', () => {
  let service: AuthService;
  let model: Model<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: getModelToken('User'),
          useValue: {
            findOne: jest.fn().mockResolvedValue(mockSavedUser),
            findByIdAndUpdate: jest.fn().mockResolvedValue(mockSavedUser),
            create: jest.fn(),
            find: jest.fn(),
            exec: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    model = module.get<Model<User>>(getModelToken('User'));
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('findByLogin', async () => {
    jest.spyOn(model, 'find').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce(mockSavedUser),
    } as any);

    const user = await service.findByLogin(registerUserDto);

    expect(user).toEqual(mockSavedUser);
  });

  it('findById', async () => {
    jest.spyOn(model, 'find').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce(mockSavedUser),
    } as any);

    const id = '63e2299315370734507eeb20';
    const password = 'sdsdsd12312wqe312';

    const user = await service.findById(id, password);

    expect(user).toEqual(mockSavedUser);
  });

  it('changePassword', async () => {
    jest.spyOn(model, 'find').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce(mockSavedUser),
    } as any);

    const id = '63e2299315370734507eeb20';

    const user = await service.changePassword(id, {
      oldPassword: 'sdsdsd12312wqe312',
      password: 'sdsdsd12312wqe312',
    });

    expect(user).toBe(responses.updatedPassword);
  });

  it('create', async () => {
    jest.spyOn(model, 'create').mockReturnValue({
      exec: jest.fn().mockResolvedValue(mockCreateUser),
    } as any);
    await expect(service.create(mockCreateUser)).rejects.toThrow(
      new HttpException(responses.existsUser, HttpStatus.BAD_REQUEST),
    );
  });
});
