import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from '../auth.controller';
import { AuthService } from '../auth.service';
import { ConfigModule } from '@nestjs/config';
import { UserSchema } from '../schemas/user.schema';
import { RegisterDto } from '../dto/register.dto';

describe('Cats Controller', () => {
  let controller: AuthController;
  let service: AuthService;

  const mockPayload = {
    email: 'ilyasd@gmail.com',
  };

  const mockSavedUser = {
    _id: '63e2299315370734507eeb20',
    email: 'ilyasdqweqwe@gmail.com',
    password: '$2a$10$Y4qA2jDrPweo5479.qTovOeM.zc/b5wE.llxfHjHwkBkUABCrTYcG',
    __v: 0,
  };

  const registerUserDto: RegisterDto = {
    email: 'ilyasd@gmail.com',
    password: 'sdsdsd12312312',
  };

  const mockAuthUser = {
    user: {
      _id: '63e1116567d093412256a1c6',
      email: 'ilyasd@gmail.com',
      __v: 0,
    },
    token: 'asdasdasd.asdasd.asdasd-asdasd',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(),
        MongooseModule.forRootAsync({
          useFactory: () => ({
            uri: process.env.MONGO_URI,
          }),
        }),
        MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.register({
          secret: process.env.JWT_SECRET,
          signOptions: { expiresIn: '1d' },
        }),
      ],
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: {
            create: jest.fn().mockResolvedValue(registerUserDto),
            registerPayload: jest.fn().mockResolvedValue(mockPayload),
            findByLogin: jest.fn().mockResolvedValue(registerUserDto),
          },
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    service = module.get<AuthService>(AuthService);
  });

  it('should signup new user', async () => {
    const createSpy = jest
      .spyOn(service, 'create')
      .mockResolvedValueOnce(mockAuthUser as any);

    await controller.register(registerUserDto);
    expect(createSpy).toHaveBeenCalledWith(registerUserDto);
  });

  it('should login user', async () => {
    const createSpy = jest
      .spyOn(service, 'findByLogin')
      .mockResolvedValueOnce(mockSavedUser as any);

    await controller.login(registerUserDto);
    expect(createSpy).toHaveBeenCalledWith(registerUserDto);
  });
});
