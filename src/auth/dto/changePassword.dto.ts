import { IsString, Matches, IsNotEmpty } from 'class-validator';
import { responses } from '../../constants/responses';

export class ChangePasswordDto {
  @IsString()
  @IsNotEmpty()
  readonly oldPassword: string;

  @IsString()
  @IsNotEmpty()
  @Matches(
    /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/,
    { message: responses.weakPassword },
  )
  readonly password: string;
}
