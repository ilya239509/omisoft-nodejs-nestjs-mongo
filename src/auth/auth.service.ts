import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { sign } from 'jsonwebtoken';
import { ChangePasswordDto } from './dto/changePassword.dto';
import { responses } from '../constants/responses';
import { IUser, ICreatedUser } from 'src/types/user';
import { RegisterDto } from './dto/register.dto';
import { LoginDTO } from './dto/login.dto';
import { Password } from '../utils/crypt';
import { UserDoc } from './schemas/user.schema';

interface Payload {
  email: string;
}

@Injectable()
export class AuthService {
  constructor(@InjectModel('User') private userModel: Model<UserDoc>) {}

  async registerPayload(payload: Payload) {
    return sign(payload, process.env.SECRET_KEY, { expiresIn: '1d' });
  }

  async create(registerDto: RegisterDto): Promise<ICreatedUser> {
    const { email, password } = registerDto;
    const user = await this.userModel.findOne({ email });

    if (user) {
      throw new HttpException(responses.existsUser, HttpStatus.BAD_REQUEST);
    }
    const hashedPassword = await Password.hashPassword(password);

    const createdUser: ICreatedUser = await this.userModel.create({
      email,
      password: hashedPassword,
    });

    return createdUser;
  }
  async findByPayload(payload: { email: string }): Promise<ICreatedUser> {
    const { email } = payload;
    return await this.userModel.findOne({ email });
  }

  async findByLogin(UserDTO: LoginDTO): Promise<ICreatedUser> {
    const { email, password } = UserDTO;

    const user = await this.userModel.findOne({ email });

    if (!user) {
      throw new HttpException(responses.invalidCredentials, HttpStatus.BAD_REQUEST);
    }

    const comparePassword = await Password.comparePassword(
      password,
      user.password,
    );

    if (comparePassword) {
      return user;
    } else {
      throw new HttpException(
        responses.invalidCredentials,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async findById(id: string, password: string): Promise<ICreatedUser> {
    const user = await this.userModel.findOne({ _id: id });
    if (!user) {
      throw new HttpException(responses.invalidCredentials, HttpStatus.BAD_REQUEST);
    }
    const comparePassword = await Password.comparePassword(
      password,
      user.password,
    );
    if (comparePassword) {
      return user;
    } else {
      throw new HttpException(
        responses.invalidCredentials,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  async update(id: string, payload: Partial<IUser>): Promise<ICreatedUser> {
    return await this.userModel.findByIdAndUpdate({ _id: id }, payload, {
      new: true,
    });
  }

  async validateUser(payload: Payload): Promise<ICreatedUser> {
    return await this.findByPayload(payload);
  }

  async changePassword(
    userId: string,
    changePasswordDto: ChangePasswordDto,
  ): Promise<string> {
    await this.findById(userId, changePasswordDto.oldPassword);
    const password = await Password.hashPassword(changePasswordDto.password);

    const updatedUser = await this.update(userId, { password });
    if (!updatedUser) {
      throw new HttpException(
        responses.updatedPasswordError,
        HttpStatus.BAD_REQUEST,
      );
    }
    return responses.updatedPassword;
  }
}
