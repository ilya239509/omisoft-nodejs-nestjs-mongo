import {
  Body,
  Controller,
  Post,
  Put,
  UseGuards,
  ValidationPipe,
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport/dist/auth.guard";
import { GetUser } from "../decorators/get-user.decorator";
import { IUser } from "../types/user";
import { RegisterDto } from "./dto/register.dto";
import { AuthService } from "./auth.service";
import { ChangePasswordDto } from "./dto/changePassword.dto";
import { LoginDTO } from "./dto/login.dto";
import { Auth } from "../constants/apiRoutes";

@Controller(Auth.Base)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post(Auth.Routes.Register)
  async register(@Body() registerDto: RegisterDto) {
    const { email, _id, __v } = await this.authService.create({
      email: registerDto.email.toLowerCase(),
      password: registerDto.password,
    });

    const payload = {
      email: email,
    };

    const token = await this.authService.registerPayload(payload);
    return { user: { email, _id, __v }, token };
  }
  @Post(Auth.Routes.Login)
  async login(@Body() loginDTO: LoginDTO) {
    const { email, _id, __v } = await this.authService.findByLogin({
      email: loginDTO.email.toLowerCase(),
      password: loginDTO.password,
    });
    const payload = {
      email: email,
    };
    const token = await this.authService.registerPayload(payload);
    return { user: { email, _id, __v }, token };
  }
  @Put(Auth.Routes.ChangePassword)
  @UseGuards(AuthGuard())
  async changePassword(
    @GetUser() user: IUser,
    @Body(new ValidationPipe()) changePasswordDto: ChangePasswordDto
  ): Promise<string> {
    return this.authService.changePassword(user._id, changePasswordDto);
  }
}
