export const responses = {
  weakPassword: 'Weak password',
  updatedPassword: 'Password has been updated',
  updatedPasswordError: `Password hasn't been updated`,
  unauthorizedAccess: 'Unauthorized access',
  existsUser: 'user already exists',
  invalidCredentials: 'invalid credentials',
};
