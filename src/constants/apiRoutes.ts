export const Auth = {
  Base: '/auth',
  Routes: {
    Register: '/register',
    Login: '/login',
    ChangePassword: '/change-password',
  },
};
