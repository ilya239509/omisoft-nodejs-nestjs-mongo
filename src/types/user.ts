import mongoose, { Document }  from 'mongoose';

export interface IUser extends Document {
  email: string;
  password: string;
}

export interface IRegister extends Document {
  user: {
    _id: mongoose.Types.ObjectId;
    email: string;
    __v: number;
  };
  token: string;
}

export interface ICreatedUser extends Document {
  _id: string | mongoose.Types.ObjectId;
  email: string;
  password: string;
  __v?: number;
}
